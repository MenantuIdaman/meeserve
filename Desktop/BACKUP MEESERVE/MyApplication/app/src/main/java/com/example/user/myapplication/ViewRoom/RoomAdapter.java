package com.example.user.myapplication.ViewRoom;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by albazzy on 15/04/2017.
 */

public class RoomAdapter extends RecyclerView.Adapter<RoomAdapter.ViewHolder>{
    List<Room>itemList;
    private Context context;

    public RoomAdapter(List<Room> itemList, Context context) {
        this.itemList = itemList;
        this.context = context;

    }



    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.room_container,parent,false);
        return  new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
    final Room datas = itemList.get(position);
        Picasso.with(context).load(datas.getImage()).into(holder.image);
        holder.txtname.setText(datas.getName());
        //holder.txtdesc.setText(datas.getDesc());
        holder.txtcaps.setText(datas.getCapacity());
        holder.txtfloor.setText(datas.getFloor());
        holder.txtfacility.setText(datas.getFacility());



    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView image;
        public TextView txtname;
        public TextView txtdesc;
        public TextView txtfloor;
        public TextView txtcaps;
        public TextView txtfacility;


        public RelativeLayout realive;


        public ViewHolder(View itemView)
        {
            super(itemView);
            image = (ImageView)itemView.findViewById(R.id.imageView);
            txtname = (TextView)itemView.findViewById(R.id.name);
            //txtdesc = (TextView)itemView.findViewById(R.id.desc);
            txtfloor = (TextView)itemView.findViewById(R.id.floor);
            txtcaps = (TextView)itemView.findViewById(R.id.capacity);
            txtfacility= (TextView)itemView.findViewById(R.id.facility);

            realive = (RelativeLayout)itemView.findViewById(R.id.relativelayout);
        }
    }
}
