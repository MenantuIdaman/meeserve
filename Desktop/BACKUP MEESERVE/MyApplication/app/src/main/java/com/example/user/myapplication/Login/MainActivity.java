package com.example.user.myapplication.Login;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputFilter;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.user.myapplication.BookingRoomVerif.BookingRoomVerifActivity;
import com.example.user.myapplication.LainLain.connectionDetector;
import com.example.user.myapplication.Menu.menuActivity;
import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.user.myapplication.LainLain.URL.url;
//import static com.example.user.myapplication.LainLain.URL.url2;

//import com.androidquery.AQuery;
//import com.androidquery.callback.AjaxCallback;
//import com.androidquery.callback.AjaxStatus;
//import org.apache.http.HttpEntity;

public class MainActivity extends AppCompatActivity {


    String error;
    String sts;
    EditText txtEmail, txtPassword;
    String email, password;
    connectionDetector cd;
    Boolean isInternetPresent = false;
    List<HashMap<String, String>> list_data = new ArrayList<HashMap<String, String>>();

    String useridget, usernameget, emailget, peopleidget, nameget;
    String name, userid;

    ProgressDialog mProgressDialog;

    Button btnLogin;

    UserSessionManager session;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //SESSION
        session = new UserSessionManager(getApplicationContext());

        if (session.isUserLoggedIn() == true) {
            session.getUserDetails();
            Intent intent = new Intent(getApplicationContext(), menuActivity.class);
            startActivity(intent);
        } else {
            getApplicationContext();
        }
        //inisialisasi... nunjuk dari front end ke back end
        btnLogin = (Button) findViewById(R.id.btnLogin);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtPassword = (EditText) findViewById(R.id.txtPassword);

        txtEmail.setFilters(new InputFilter[]{new InputFilter.AllCaps()});

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new connectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    email = txtEmail.getText().toString();
                    password = txtPassword.getText().toString();
                    if (email.equals("")) {
                        Toast.makeText(getApplicationContext(), "Please fill your username", Toast.LENGTH_LONG).show();
                    } else if (password.equals("")) {
                        Toast.makeText(getApplicationContext(), "Please fill your password", Toast.LENGTH_LONG).show();
                    } else {
                        kirimlogin(email, password);
                    }
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }


            }
        });

    }


    private void kirimlogin(final String em, String pw) {
        class LoginAsync extends AsyncTask<String, Void, JSONObject> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(MainActivity.this);
                mProgressDialog.setMessage("Loading...");
                //mProgressDialog.setIndeterminate(false);
                mProgressDialog.setCancelable(false);
                // mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
//                loadingDialog = ProgressDialog.show(getApplicationContext(), "Harap tunggu", "Loading...");
            }

            @Override
            protected JSONObject doInBackground(String... params) {
                String responce;
                //
                //                String uid = params[0];
                String eml = params[0];
                String pwd = params[1];


                JSONObject param = new JSONObject();

                try {

                    //                    param.put("sessionId", sid);
                    param.put("username", eml);
                    param.put("password", pwd);

                } catch (Exception ex) {

                }
                //                postData(Config.url+"/login", param);
                //                ceklogin();
                String message = "";

                message = param.toString();
                Log.d("header", param.toString());

                try {

                    //                    HttpPost httppost = new HttpPost(url.toString());
                    HttpClient httpClient = new DefaultHttpClient();
                    //                    HttpPost httpPost = new HttpPost(url_test+"/login");
                    HttpPost httpPost = new HttpPost(url + "login.php");
                    //                    HttpPost httpPost = new HttpPost(url+"/login");
                    httpPost.setEntity(new StringEntity(message, "UTF8"));
                    //                    httpPost.setHeader("Accept", "application/json");
                    //                    httpPost.setHeader("userId", userId);
                    //                    httpPost.setHeader("sessionId", sessionId);
//                    httpPost.setHeader("api", api_login);
//                    httpPost.setHeader("path", path);
//                    httpPost.setHeader("apikey", apikey);
                    httpPost.setHeader("Content-type", "application/json");
                    //                    httpPost.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));

                    StringEntity se = new StringEntity(param.toString());
                    //                    se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                    httpPost.setEntity(se);

                    //                    Log.d("hello",sessionId);

                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();

                    responce = EntityUtils.toString(entity);
                    Log.d("response is", responce);

                    //                    String temp = EntityUtils.toString(response.getEntity());
                    //                    Log.i("tag", temp);

                    return new JSONObject(responce);

                    //                    ======================================

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }


            @Override
            protected void onPostExecute(JSONObject result) {
                super.onPostExecute(result);
                try {
                    mProgressDialog.dismiss();

//                    JSONObject jsonUserData = result.getJSONObject("user_data");
//                    //JSONObject jsonError = jsonUserData.getJSONObject("error_msg");
//                    JSONObject jsonData = jsonUserData.getJSONObject("data");
//
//                    //untuk nangkep error
//                    error = jsonUserData.getString("error_msg");
//                    userid = jsonData.getString("userid");
//                    name = jsonData.getString("name");
//                    String username = jsonData.getString("username");
//                    String email = jsonData.getString("useremail");
//
//                    HashMap<String, String> map = new HashMap<String, String>();
//
//                    map.put("userid", userid);
//                    map.put("name", name);
//                    map.put("username", username);
//                    map.put("useremail", email);
//                    map.put("error_msg", error);
//
//                    list_data.add(map);
//
//                    //untuk ambil data user

                    JSONObject v = result.getJSONObject("user_data");
//                    userid = v.getString("userid");
//                    name = v.getString("name");
//                    String username = v.getString("name");
//                    String email = v.getString("useremail");
                    error = v.getString("error_msg");

                   if (error.equals("true")) {
                       session.createUserLoginSession(v.getString("name"), v.getString("userid"));
                       Intent intent = new Intent(getApplicationContext(),menuActivity.class);
                       startActivity(intent);
                       finish();
//                        HashMap<String, String> map = new HashMap<String, String>();
//
//                    map.put("userid", v.getString("userid"));
//                    map.put("name",v.getString("name"));
//                    map.put("username",v.getString("username"));
//                    map.put("useremail", v.getString("useremail"));
//                    //map.put("peopleid", peopleid);
//                    map.put("error_msg",v.getString("error_msg"));
//
//                    list_data.add(map);
                    }else if (error.equals("false")){
                       Toast.makeText(getApplicationContext(), "Wrong NPP or Password", Toast.LENGTH_LONG).show();

                   }


//                    HashMap<String, String> map = new HashMap<String, String>();
//
//                    map.put("userid", userid);
//                    map.put("name",name);
//                    map.put("username", username);
//                    map.put("useremail", email);
//                    //map.put("peopleid", peopleid);
//                    map.put("error_msg",error);

//                    list_data.add(map);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                cekerror();
            }
        }
        LoginAsync reg = new LoginAsync();
        reg.execute(em, pw);
    }


    public void cekerror() {
        for (int i = 0; i < list_data.size(); i++) {
            HashMap<String, String> dat = list_data.get(i);
            nameget = dat.get("name");
            useridget = dat.get("userid");
            usernameget = dat.get("username");
            emailget = dat.get("useremail");
            error = dat.get("error_msg");


            if (error.equals("true")) {
                session.createUserLoginSession(nameget, useridget);
                Intent intent = new Intent(getApplicationContext(),menuActivity.class);
                startActivity(intent);
                finish();
            } else if (error.equals("false")) {
                Toast.makeText(getApplicationContext(), "Wrong NPP or Password", Toast.LENGTH_LONG).show();

            }
        }
    }




    public void showAlert(final String pesan){
        runOnUiThread(new Runnable() {
            public void run() {
                // TODO Auto-generated method stub
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setTitle("Error");
                builder.setMessage(pesan).setCancelable(false).setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // TODO Auto-generated method stub

                    }
                });

                final AlertDialog alert = builder.create();
                alert.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
                        alert.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                    }
                });
                alert.show();
            }
        });
    }

}

