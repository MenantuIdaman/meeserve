package com.example.user.myapplication.Menu;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.myapplication.BookiingDetail.detailActivity;
import com.example.user.myapplication.History.MainActivity;
import com.example.user.myapplication.LainLain.connectionDetector;
import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;
import com.example.user.myapplication.Setting.SettingActivity;

import java.util.HashMap;
//import com.example.user.myapplication.ViewRoomLama.viewroomActivity;


/**
 * Created by endis on 09/03/2017.
 */

public class menuActivity extends AppCompatActivity {
        connectionDetector cd;
        Boolean isInternetPresent = false;

        Button room, booking,history;
        ImageButton setting;
        String name,userid;

        String namaget,userget;
        UserSessionManager session;
        boolean doubleBackToExitPressedOnce = false;

        @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.menu);
            Toolbar toolbar = (Toolbar)findViewById(R.id.MenuToolbar) ;
            setSupportActionBar(toolbar);
            toolbar.setTitle("Meeserve BNI");
            toolbar.setTitleTextColor(Color.WHITE);

            Intent intent = getIntent();
//            name= intent.getStringExtra("name");
//            userid=intent.getStringExtra("userid");

            TextView user_name =(TextView)findViewById(R.id.user_name);

//            Bundle bd = intent.getExtras();

//            String getUserName=(String) bd.get("name");




            session = new UserSessionManager(getApplicationContext());
            HashMap<String,String> user = session.getUserDetails();
            namaget = user.get(UserSessionManager.KEY_NAME);
            userget = user.get(UserSessionManager.KEY_UserId);

            user_name.setText(namaget);

            //Toast.makeText(getApplicationContext(), namaget+"\n"+userget, Toast.LENGTH_LONG).show();

            //membuat insisalisasi button2 yang ada
            room = (Button) findViewById(R.id.room);

            booking = (Button) findViewById(R.id.booking);
            //aboutUs = (Button) findViewById(R.id.aboutUs);
            //logOut = (Button) findViewById(R.id.logOut);
            history = (Button)findViewById(R.id.history) ;

            setting = (ImageButton)findViewById(R.id.setting);

            room.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    cd= new connectionDetector(getApplicationContext());
                    isInternetPresent=cd.isConnectingToInternet();
                    if (isInternetPresent){
                        Intent i1 = new Intent(menuActivity.this, com.example.user.myapplication.ViewRoom.MainActivity.class);
                        startActivity(i1);
                    }else{
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                    }

                }

            });



            booking.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    cd= new connectionDetector(getApplicationContext());
                    isInternetPresent=cd.isConnectingToInternet();
                    if (isInternetPresent){
                        Intent i1 = new Intent(menuActivity.this, detailActivity.class);
                        //i1.putExtra("userid",userid);
                        startActivity(i1);
                    }else{
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                    }

                }

            });

            history.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    cd= new connectionDetector(getApplicationContext());
                    isInternetPresent=cd.isConnectingToInternet();
                    if (isInternetPresent){
                        Intent i1 = new Intent(menuActivity.this, MainActivity.class);
                        startActivity(i1);
                    }else{
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                    }

                }

            });



            setting.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v){
                    cd= new connectionDetector(getApplicationContext());
                    isInternetPresent=cd.isConnectingToInternet();
                    if (isInternetPresent){
                        Intent i1 = new Intent(menuActivity.this, SettingActivity.class);
                        //i1.putExtra("userid",userid);
                        startActivity(i1);
                    }else{
                        Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                    }

                }

            });

        }



    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            menuActivity.this.finishAffinity();
            return;

        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }



}
