package com.example.user.myapplication.History;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.example.user.myapplication.R;

import java.util.ArrayList;

public class Complete_Detail extends AppCompatActivity {
    String trid;
    String error;
    ProgressDialog pd;
    ListView listView;
    ArrayList<AttendanceModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_complete__detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        trid = intent.getStringExtra("trid");

        if (getIntent().getSerializableExtra("tr_status") != null) {
            BookedModel history = (BookedModel) getIntent().getSerializableExtra("tr_status");


            TextView status2 = (TextView) findViewById(R.id.status2);
            status2.setText(history.Status);
            TextView agenda = (TextView) findViewById(R.id.agenda2);
            agenda.setText(history.Agenda);
            final TextView start = (TextView) findViewById(R.id.starthour);
            start.setText(history.Start_hour);
            TextView end = (TextView) findViewById(R.id.endhour);
            end.setText(history.end_hour);
            TextView booked = (TextView) findViewById(R.id.booked2);
            booked.setText(history.booked_date);
            TextView bookingnum = (TextView) findViewById(R.id.bookingnumber);
            bookingnum.setText(history.bookingnumber);
            TextView bookingdate = (TextView) findViewById(R.id.bookingdates1);
            bookingdate.setText(history.booking_time);
            TextView roomname = (TextView) findViewById(R.id.ruangan2);
            roomname.setText(history.Room_name);
            TextView id = (TextView) findViewById(R.id.IdPass);
            id.setText(history.tr_id);
            TextView mom = (TextView)findViewById(R.id.MOM);
            mom.setText(history.summary);

        }

       TextView see = (TextView)findViewById(R.id.see_Att_button);
        see.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(Complete_Detail.this,Attendee_List.class);
                z.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                z.putExtra("trid",trid);
                startActivity(z);
            }
        });

    }





}