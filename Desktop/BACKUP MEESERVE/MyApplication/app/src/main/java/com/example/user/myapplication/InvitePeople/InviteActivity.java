package com.example.user.myapplication.InvitePeople;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.myapplication.BookiingDetail.ListViewModelBooking;
import com.example.user.myapplication.Menu.menuActivity;
import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static com.example.user.myapplication.LainLain.URL.url;


public class InviteActivity extends AppCompatActivity {

    connectionDetector check;
    Boolean isInternetPresent = false;

    String mulai, selesai, date, userid, roomID,roomCapacity;

    ArrayAdapter<String> adapter;
    EditText editText;
    ArrayList<String> itemList;
    JSONObject jsonObject;

    ListViewModel data = new ListViewModel();
    //buat asynctask
    String sts;
    String namaPeg;
    String email, password;
    Context ctx = this;
    String PASSWORD = null, EMAIL = null;
    connectionDetector cd;
    ArrayList<ListViewModel> listNama;

    List<HashMap<String, String>> list_data = new ArrayList<HashMap<String, String>>();
    String nama;
    String judul;
    String error;
    String userget;

    Button btnSendEmail;
    List<ListViewModel> rowItems;


    List<String> responseList;
    ProgressDialog mProgressDialog;

    UserSessionManager session;

    int roomCapacityInteger;

    int list;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.invite);
        Toolbar toolbar = (Toolbar)findViewById(R.id.AgendaToolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Meeting Agenda");
        toolbar.setTitleTextColor(Color.WHITE);

        final String name[] = {};

        itemList = new ArrayList<String>(Arrays.asList(name));
        adapter = new ArrayAdapter<String>(this, R.layout.list_item, R.id.emailView, itemList);
        final ListView listV = (ListView) findViewById(R.id.listEmail);

        listV.setAdapter(adapter);


        final EditText agenda = (EditText) findViewById(R.id.agenda);
        Button addName = (Button) findViewById(R.id.addName);
        btnSendEmail = (Button) findViewById(R.id.sendEmail);

        rowItems = new ArrayList<ListViewModel>();


        Intent intent = getIntent();
        mulai = intent.getStringExtra("start");
        selesai = intent.getStringExtra("end");
        date = intent.getStringExtra("bookingdate");
        roomID = intent.getStringExtra("roomid");
        roomCapacity = intent.getStringExtra("room_caps");

        roomCapacityInteger = Integer.parseInt(roomCapacity);

        //userid = intent.getStringExtra("userid");
//        Toast.makeText(getApplicationContext(),roomCapacityInteger,Toast.LENGTH_LONG).show();
//        Toast.makeText(this,String.valueOf(roomCapacityInteger),Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(),  userget + "\n"+mulai + "\n" + selesai + "\n" + date + "\n"  + roomID, Toast.LENGTH_LONG).show();

        mProgressDialog = new ProgressDialog(InviteActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setCanceledOnTouchOutside(false);


        session = new UserSessionManager(getApplicationContext());
        HashMap<String,String> user = session.getUserDetails();
        userget = user.get(UserSessionManager.KEY_UserId);

        //NewAutoComplete

        final AutoCompleteTextView namaInvite = (AutoCompleteTextView) findViewById(R.id.invEmail);
        namaInvite.setAdapter(new SuggestionAdapter(this, namaInvite.getText().toString()));



        addName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check= new connectionDetector(getApplicationContext());
                isInternetPresent=check.isConnectingToInternet();
                if (isInternetPresent){
                    if(namaInvite.getText().toString().isEmpty()) {
                        Toast.makeText(InviteActivity.this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                    }else if(rowItems.size() >= roomCapacityInteger) {
                        Toast.makeText(InviteActivity.this, "Your invitation list more than your room capacity", Toast.LENGTH_LONG).show();
//                    }else if (listV.getAdapter().getCount()==0 ){
//                    }else if (list==0 ){
//                        Toast.makeText(InviteActivity.this, "You must invite at least 1 person", Toast.LENGTH_LONG).show();
                    }else{
                        String newItem=namaInvite.getText().toString();
                        //MISAHIN ANTARA 2 ARRAY DARI ADAPTER
                        String[] parts=newItem.split("-");
                        //NAMPUNG ARRAY NAMA SAMA ID YG UDH DI PISAH
                        if(parts.length==1){
                            Toast.makeText(InviteActivity.this, "Name cannot be empty", Toast.LENGTH_SHORT).show();
                        }else {
                            String nama=parts[0];
                            String id=parts[1];
                            // add new item to arraylist
                            itemList.add(nama);

                            ListViewModel data=new ListViewModel();
                            //SET NAMA
                            data.setInvitedName(nama);
                            //SET ID
                            data.setId(id);
                            rowItems.add(data);

                            //kata stack overflow errornya di set text ini.
                            //namaInvite.setText("");
                            namaInvite.getText().clear();

                            // notify listview of data changed
                            adapter.notifyDataSetChanged();
                        }

                    }

                }else{
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }


        });

        btnSendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                check= new connectionDetector(getApplicationContext());
                isInternetPresent=check.isConnectingToInternet();
                list = adapter.getCount();
                if (isInternetPresent){
                    if(agenda.getText().toString().isEmpty()){
                        Toast.makeText(InviteActivity.this, "Headline cannot be empty", Toast.LENGTH_SHORT).show();
                    }else if (list==0 ){
                        Toast.makeText(InviteActivity.this, "You must invite at least 1 person", Toast.LENGTH_LONG).show();
                    }else{
                        judul = agenda.getText().toString();
                        kirimData(roomID,userget,date,judul,mulai,selesai);
                        //Toast.makeText(InviteActivity.this, String.valueOf(list), Toast.LENGTH_LONG).show();

                    }
                }else{
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    //SAMPE SINI AUTOCOMPLETE

    private void kirimData(final String jdl,String ml, String sls, String tgl, String roi, String usid) {
        ArrayList<ListViewModel> listNama = new ArrayList<ListViewModel>();
        for (int i = 0; i < rowItems.size(); i++) {

            ListViewModel model = rowItems.get(i);
            String k = model.getInvitedName();
            String ab = "";

        }

        class InviteAsync extends AsyncTask<String, Void, JSONObject> {

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                mProgressDialog.show();

                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
            }

            @Override
            protected JSONObject doInBackground(String... params) {
                String responce;
//                String nama = params[0];
                String rid = params[0];
                String uid = params[1];
                String dt = params[2];
                String judul = params[3];
                String st = params[4];
                String ed = params[5];

                JSONObject param = new JSONObject();
                JSONObject param2 = new JSONObject();
                JSONArray detail = new JSONArray();

//                JSONObject jsonRoot = new JSONObject();
//                JSONArray jsonAkhir = new JSONArray();
                try {
                    ArrayList<ListViewModel> listNama = new ArrayList<ListViewModel>();
//                    JSONArray jsonArray = new JSONArray(Arrays.asList(itemList));
                    param.put("roomid", rid);
                    param.put("userid", uid);
                    param.put("bookingdate", dt);
                    param.put("agenda", judul);
                    param.put("start", st);
                    param.put("end", ed);

                    for (int i = 0; i < rowItems.size(); i++) {
//                      for (int i = 0; i <= roomCapacityInteger; i++) {
                        JSONObject param3 = new JSONObject();
                        ListViewModel modelNama = rowItems.get(i);
                        //String k=modelNama.getInvitedName();
                        param3.put("npp", modelNama.getID());
                        param3.put("nama", modelNama.getInvitedName());
                        detail.put(i, param3);

                    }
                    param.put("invited", detail);

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                String message = "";

                message = param.toString();
                Log.d("header", param.toString() + param2.toString());


                try {

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"insertBooking3.php");
                    httpPost.setEntity(new StringEntity(message, "UTF8"));
                    httpPost.setHeader("Content-type", "application/json");

                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();

                    responce = EntityUtils.toString(entity);
                    Log.d("response is", responce);

                    return new JSONObject(responce);

                    //                    ======================================

                } catch (Exception e) {
                    e.printStackTrace();
                }


                return null;
            }

            @Override
            protected void onPostExecute(JSONObject result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                if (result != null) {
                    try {
                        JSONObject v = result.getJSONObject("data");
                        error = v.getString("error_msg");


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    cekerror();

                }

            }
        }
        InviteAsync reg = new InviteAsync();
        reg.execute(jdl,ml, sls, tgl, roi, usid);
    }


    public void cekerror() {

            if(error.equals("true")){

                Intent intent = new Intent(getApplicationContext(), menuActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                finish();
                startActivity(intent);
                Toast.makeText(InviteActivity.this, "Your Transaction Has Been Recorded", Toast.LENGTH_SHORT).show();
            }else{
                Toast.makeText(InviteActivity.this, "Your Transaction Not Recorded Because Internal Error, Please Try Again Later", Toast.LENGTH_SHORT).show();
            }
        }

}
