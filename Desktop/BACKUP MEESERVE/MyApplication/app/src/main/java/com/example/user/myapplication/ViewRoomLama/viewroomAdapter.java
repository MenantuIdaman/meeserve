package com.example.user.myapplication.ViewRoomLama;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.user.myapplication.R;

import java.util.List;

/**
 * Created by AASUS on 22/03/2017.
 */

public class viewroomAdapter extends BaseAdapter {

    private Activity activity;
    private List<viewroomModel> model;
    private LayoutInflater inflater;

    public viewroomAdapter(Activity activity, List<viewroomModel> model){
        this.activity = activity;
        this.model = model;
    }

    public static class ViewHolder{
        protected TextView textRoomid;
        protected TextView textRoomname;
        protected TextView textFloor;
    }

    @Override
    public int getCount() {
        return model.size();
    }

    @Override
    public Object getItem(int position) {
        return model.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if(convertView == null){
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.list_single, null);
            viewHolder = new ViewHolder();
            viewHolder.textRoomid=(TextView) convertView.findViewById(R.id.txt1);
            viewHolder.textRoomname=(TextView) convertView.findViewById(R.id.txt2);
            viewHolder.textFloor = (TextView) convertView.findViewById(R.id.img);

            convertView.setTag(viewHolder);
            convertView.setTag(R.id.txt1, viewHolder.textRoomid);
            convertView.setTag(R.id.txt2, viewHolder.textRoomname);
            //convertView.setTag(R.id.img, viewHolder.textFloor);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.textRoomid.setText(model.get(position).getRoomid());
        viewHolder.textRoomname.setText(model.get(position).getName());
        viewHolder.textFloor.setText(model.get(position).getName());
//        viewHolder.imageFloor.setImageResource(Integer.parseInt(model.get(position).getFloor()));

        return convertView;
    }
}
