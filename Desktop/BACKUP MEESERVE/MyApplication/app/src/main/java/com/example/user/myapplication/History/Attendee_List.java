package com.example.user.myapplication.History;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ListView;

import com.example.user.myapplication.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.example.user.myapplication.LainLain.URL.url;

public class Attendee_List extends AppCompatActivity {

    String trid;
    String error;
    ProgressDialog pd;
    ListView listView;
    ArrayList<AttendanceModel> data;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_attendee__list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Attendee List");
        toolbar.setTitleTextColor(Color.WHITE);

        Intent intent = getIntent();
        trid = intent.getStringExtra("trid");
        listView = (ListView)findViewById(R.id.item_alternative) ;
        data = new ArrayList<>();
        cancelBooking(trid);

    }
    private void cancelBooking(final String tr)
    {
        class cancelAsync extends AsyncTask<String,String,JSONObject>
        {

            protected void onPreExecute()
            {
                super.onPreExecute();
                pd = new ProgressDialog(Attendee_List.this);
                pd.setMessage("Loading...");
                pd.setIndeterminate(false);
                pd.show();

            }

            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"absentPeople.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("response is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);
                pd.dismiss();
                if(result!=null)


                //JSONArray jsonArray = jsonObject.getJSONArray("history_data");
                {
                    try {

                        JSONArray jsonArray = result.getJSONArray("attendee_data");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            data.add(new AttendanceModel(
                                    jsonObject.getString("name"),
                                    jsonObject.getString("People_Email"),
                                    jsonObject.getString("Position_Name"),
                                    jsonObject.getString("Attendee_Desc")




                            ));
                            error = jsonObject.getString("error_msg");


                        }
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                    }

                    AttendanceAdaptor adapterAttendance = new AttendanceAdaptor(getApplicationContext(),R.layout.attendee_list,data);
                    listView.setAdapter(adapterAttendance);

                }
            }

        }
        cancelAsync async = new cancelAsync();
        async.execute(tr);

    }

}
