package com.example.user.myapplication.Booking;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import android.app.Activity;

import com.example.user.myapplication.R;
import com.example.user.myapplication.ViewRoomLama.customListStatic;

public class bookingActivity extends Activity {

    ListView list;
    String[] web = {
            "RUANG LONDON\n" +
                    "Spesifikasi:\n" +
                    " -AC\n" +
                    " -OHP\n" +
                    " -10 person",
            "RUANG PARIS\n" +
                    "Spesifikasi:\n" +
                    " -AC\n" +
                    " -OHP\n" +
                    " -10 person",
            "RUANG MILAN\n" +
                    "Spesifikasi:\n" +
                    " -AC\n" +
                    " -OHP\n" +
                    " -10 person",
            "RUANG BARCELONA\n" +
                    "Spesifikasi:\n" +
                    " -AC\n" +
                    " -OHP\n" +
                    " -10 person"

    };
    Integer[] imageId = {
            R.drawable.ruang1,
            R.drawable.ruang2,
            R.drawable.ruang3,
            R.drawable.creativelightmeetingroom


    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.booking);

        customListStatic adapter = new
                customListStatic(bookingActivity.this, web, imageId);
        list=(ListView)findViewById(R.id.listRoom);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                Toast.makeText(bookingActivity.this, "You Clicked at " +web[+ position], Toast.LENGTH_SHORT).show();

            }
        });

    }

    }

