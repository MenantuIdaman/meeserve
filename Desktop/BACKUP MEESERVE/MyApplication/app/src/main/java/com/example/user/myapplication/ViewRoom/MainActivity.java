package com.example.user.myapplication.ViewRoom;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.user.myapplication.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.example.user.myapplication.LainLain.URL.url;

public class MainActivity extends AppCompatActivity {
   private List<Room>itemList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;

    //private static final String URL_DATA =" http://fyi.web.id/test/";
    private static final String URL_DATA = url+"room.php";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_list);
        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbarroomlist);
        setSupportActionBar(toolbar);
//        toolbar.setTitle("Room List");
        toolbar.setTitleTextColor(Color.WHITE);
        toolbar.getTitle();

        recyclerView =(RecyclerView)findViewById(R.id.recyclerviews);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        itemList = new ArrayList<>();
        loadRecyclerData();
    }

    private void loadRecyclerData()
    {
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Room");
        progressDialog.show();
        progressDialog.setCancelable(false);
        progressDialog.setCanceledOnTouchOutside(false);

        StringRequest stringRequest = new StringRequest(Request.Method.GET, URL_DATA, new Response.Listener<String>() {
            @Override
            public void onResponse(String s) {
                try {
                    JSONObject jsonObject = new JSONObject(s);
                    JSONArray jsonArray = jsonObject.getJSONArray("room_data");

                    for (int i =0;i<jsonArray.length();i++)
                    {
                        JSONObject o = jsonArray.getJSONObject(i);
                        Room data = new Room(
                                o.getString("image"),
                                o.getString("name"),
                                "Room Description : " + o.getString("desc"),
                                "Room Floor : " + o.getString("floor"),
                                "Room Facility : " + o.getString("facility"),
                                "Room Capacity : " + o.getString("capacity")


                        );
                        itemList.add(data);
                    }

                   adapter = new RoomAdapter(itemList,getApplicationContext());
                    recyclerView.setAdapter(adapter);
                    progressDialog.dismiss();

            }catch (JSONException e)
                {
                    e.printStackTrace();
                }
        }

},
        new Response.ErrorListener() {
@Override
public void onErrorResponse(VolleyError volleyError) {
        progressDialog.dismiss();
        Toast.makeText(getApplicationContext(),volleyError.getLocalizedMessage(),Toast.LENGTH_LONG).show();

        }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
        }


        }


