package com.example.user.myapplication.Absence;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.example.user.myapplication.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static com.example.user.myapplication.LainLain.URL.url;

/**
 * Created by albazzy on 08/05/2017.
 */

public class BlankPages extends Activity{

    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
    ProgressDialog pd;
    String error;
    String trid;

    SwipeRefreshLayout mSwipeRefreshLayout;

    private List<AttendanceModel> modelList;
    private RecyclerView recyclerView;
    private AttendanceAdaptor mAdapter;
    String string;
    Snackbar snackbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hello_world);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_rec);
//        setSupportActionBar(toolbar);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setTitle("Attendees");
        toolbar.setTitleTextColor(Color.WHITE);



        recyclerView =(RecyclerView)findViewById(R.id.recyclerview);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        modelList= new ArrayList<>();
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(createHelperCallBack());
        itemTouchHelper.attachToRecyclerView(recyclerView);

        Intent intent=getIntent();
        trid = intent.getStringExtra("trid");

        //Toast.makeText(getApplicationContext(),trid,Toast.LENGTH_LONG).show();
        kirimtr(trid);
        //new AsyncFetch().execute();


        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refreshAbsent);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                modelList.clear();
                snackbar.dismiss();
                kirimtr(trid);
                // new AsyncFetch().execute();
            }
        });




    }




    private ItemTouchHelper.Callback createHelperCallBack()
    {
        final ItemTouchHelper.SimpleCallback simpleCallback =
                new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP|ItemTouchHelper.DOWN,
                        ItemTouchHelper.RIGHT|ItemTouchHelper.LEFT)
                {



                    @Override
                    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                        moveItem(viewHolder.getAdapterPosition(),target.getAdapterPosition());

                        return true;
                    }



                    @Override
                    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);

                        Bitmap icon;

                        View itemView = viewHolder.itemView;
                        float height = (float) itemView.getBottom() - (float) itemView.getTop();
                        float width = height / 3;


                        if(actionState == ItemTouchHelper.ACTION_STATE_SWIPE)
                        {


                            Paint p =new Paint();
                            if(dX >0)
                            {
                                p.setColor(Color.BLUE);


                                RectF background = new RectF((float)
                                        itemView.getLeft(), (float) itemView.getTop(), dX,(float) itemView.getBottom());

                                c.drawRect(background,p);
                                icon = BitmapFactory.decodeResource(getResources(),R.drawable.ic_action_done);

                                RectF icon_dest =
                                        new RectF((float) itemView.getLeft() + width
                                                ,(float) itemView.getTop() + width
                                                ,(float) itemView.getLeft()+ 2*width,(float)itemView.getBottom() - width);

                                c.drawBitmap(icon,null,icon_dest,p);


                            }
                            else {



;                                p.setColor(Color.parseColor("#D32F2F"));
                                RectF background = new RectF((float) itemView.getRight() + dX, (float) itemView.getTop(),(float) itemView.getRight(), (float) itemView.getBottom());
                                c.drawRect(background,p);
                                icon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_action_delete);
                                RectF icon_dest = new RectF((float) itemView.getRight() - 2*width ,(float) itemView.getTop() + width,(float) itemView.getRight() - width,(float)itemView.getBottom() - width);
                                c.drawBitmap(icon,null,icon_dest,p);

                            }
                        }
                    }



                    @Override
                    public void onSwiped(final RecyclerView.ViewHolder holder, final int direction) {
                        final AttendanceModel abc=modelList.get(holder.getAdapterPosition());
                        final int position = holder.getAdapterPosition();
                        delet_item(position);



                        if(direction==ItemTouchHelper.LEFT)
                        {
                           // Toast.makeText(getApplicationContext(),"Toast LEFT " +abc.getPeopleid() ,Toast.LENGTH_SHORT).show();
                            NotAttend(abc.getTrid(),abc.getPeopleid());

                            //Snackbar snackbar = Snackbar.make(recyclerView,"Does " + abc.getAttName()+ " not attend ? ",Snackbar.LENGTH_INDEFINITE);

                            //swipe to left - not attend
                            snackbar = Snackbar.make(recyclerView,"Are you sure?",Snackbar.LENGTH_INDEFINITE);
                            final View sbview = snackbar.getView();
                            sbview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(),R.color.snackbar));
                            snackbar.setActionTextColor(Color.WHITE);
                            sbview.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                            snackbar.setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    final int mAdapterPosition = holder.getAdapterPosition();
                                    mAdapter.notifyItemChanged(mAdapterPosition);
                                    modelList.add(abc);
                                    mAdapter.notifyItemInserted(modelList.lastIndexOf(mAdapterPosition));
                                    mAdapter.onAttachedToRecyclerView(recyclerView);
                                    recyclerView.scrollToPosition(mAdapterPosition);

                                    mAdapter.notifyDataSetChanged();




                                    UndoParsing(abc.getTrid(),abc.getPeopleid());


                                }

                            });

                            snackbar.setDuration(4000);
                            snackbar.show();
//                            modelList.clear();
//                            kirimtr(trid);


                        }


                        else if(direction==ItemTouchHelper.RIGHT)
                        {
                            //Toast.makeText(getApplicationContext(),"Toast RIGHT "+abc.getPeopleid()+" "+abc.getTrid(),Toast.LENGTH_SHORT).show();
                            Attend(abc.getTrid(),abc.getPeopleid());
                            snackbar = Snackbar.make(recyclerView,"Are you sure? ",Snackbar.LENGTH_INDEFINITE);
//                            Snackbar snackbar = Snackbar.make(recyclerView,"Does " + abc.getAttName()+ " attend ? ",Snackbar.LENGTH_INDEFINITE);LENGTH_INDEFINITE

                            //swipe to right - attend
                            View sbview = snackbar.getView();
                            sbview.setBackgroundColor(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary));
                            snackbar.setActionTextColor(Color.WHITE);
                            sbview.setTextAlignment(View.TEXT_ALIGNMENT_VIEW_START);
                            snackbar.setAction("UNDO", new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    modelList.add(abc);
                                    mAdapter.notifyItemInserted(position);
                                    mAdapter.notifyItemChanged(position);
                                    mAdapter.notifyDataSetChanged();
                                    UndoParsing(abc.getTrid(),abc.getPeopleid());
                                }
                            });

                            snackbar.setDuration(4000);
                            snackbar.show();
                        }




                    }


                };



        return simpleCallback;
    }


private void addItem(int pos)
{
    AttendanceModel model = modelList.get(mAdapter.currentPos);
    modelList.add(model);
    mAdapter.notifyItemInserted(modelList.indexOf(pos));

}


    private void moveItem(int oldPos, int newPos)
    {
        AttendanceModel model = modelList.get(oldPos);
        modelList.remove(oldPos);
        modelList.add(newPos,model);
        mAdapter.notifyItemMoved(oldPos,newPos);



    }

    private void delet_item(final int pos)
    {
        modelList.remove(pos);
        mAdapter.notifyItemRemoved(pos);

    }









    private void kirimtr(final String tr) {
        class AsyncFetch extends AsyncTask<String, Void, JSONObject> {
            ProgressDialog pdLoading = new ProgressDialog(BlankPages.this);
            HttpURLConnection conn;
//            URL url = null;
            String Response;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //this method will be running on UI thread
                pdLoading.setMessage("\tLoading...");
                //pdLoading.setTitle("Parsingin Data");
                pdLoading.setCancelable(false);
                pdLoading.show();

            }

            @Override
            protected JSONObject doInBackground(String... params) {
                String responce;
                String tr_id = params[0];


                JSONObject param = new JSONObject();

                try {

                    //                    param.put("sessionId", sid);
                    param.put("trid", tr_id);

                } catch (Exception ex) {

                }
                String message = "";

                message = param.toString();
                Log.d("header", param.toString());

                try {
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"absentPeople.php");
                    httpPost.setEntity(new StringEntity(message, "UTF8"));
                    httpPost.setHeader("Content-type", "application/json");

                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();

                    responce = EntityUtils.toString(entity);
                    Log.d("response is", responce);

                    return new JSONObject(responce);

                    //                    ======================================

                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;

            }

           // @Override
            protected void onPostExecute(JSONObject result) {

                //this method will be running on UI thread

                pdLoading.dismiss();

                try {

                    JSONArray jsonArray = result.getJSONArray("attendee_data");



                    // Extract data from json and store into ArrayList as class objects
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject o = jsonArray.getJSONObject(i);
                        final AttendanceModel data = new AttendanceModel();
                        data.trid = o.getString("trid");
                        data.peopleid = o.getString("people_id");
                        data.attName = o.getString("name");
                        data.attEmail = o.getString("People_Email");
                        data.attPosisi = o.getString("Position_Name");
                        data.attDesc = o.getString("Attendee_Desc");


                        modelList.add(data);

                    }
                    // Setup and Handover data to recyclerview
                    mAdapter = new AttendanceAdaptor(modelList, getApplicationContext());
                    recyclerView.setAdapter(mAdapter);
                    mSwipeRefreshLayout.setRefreshing(false);

                } catch (JSONException e) {
                    Toast.makeText(BlankPages.this, e.toString(), Toast.LENGTH_LONG).show();
                }

            }

        }
        AsyncFetch reg = new AsyncFetch();
        reg.execute(tr);
    }





    private void NotAttend(final String tr,String mm)
    {
        class momAsync2 extends AsyncTask<String,Void,JSONObject>
        {



            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                String pid = params[1];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);
                    param.put("peopleid",pid);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"checkAbsentNotAttend.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("kirim is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);

                if(result!=null)
                {
                    try {
                        JSONObject jsonObject = result.getJSONObject("data");
                        error = jsonObject.getString("error_msg");
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                    }
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                }
            }
        }


        momAsync2 async = new momAsync2();
        async.execute(tr,mm);

    }

    private void UndoParsing(final String tr,String mm)
    {
        class momAsync3 extends AsyncTask<String,Void,JSONObject>
        {



            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                String pid = params[1];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);
                    param.put("peopleid",pid);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"checkAbsentTentative.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("kirim is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);

                if(result!=null)
                {
                    try {
                        JSONObject jsonObject = result.getJSONObject("data");
                        error = jsonObject.getString("error_msg");
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                    }
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                }
            }
        }


        momAsync3 async = new momAsync3();
        async.execute(tr,mm);

    }
    private void Attend(final String tr,String mm)
    {
        class momAsync extends AsyncTask<String,Void,JSONObject>
        {



            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                String pid = params[1];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);
                    param.put("peopleid",pid);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"checkAbsentAttend.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("response is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);

                if(result!=null)
                {
                    try {
                        JSONObject jsonObject = result.getJSONObject("data");
                        error = jsonObject.getString("error_msg");
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                    }
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                }
            }
        }


        momAsync async = new momAsync();
        async.execute(tr,mm);

    }






}



