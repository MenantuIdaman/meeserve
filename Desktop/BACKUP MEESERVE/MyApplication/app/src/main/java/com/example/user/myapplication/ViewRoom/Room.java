package com.example.user.myapplication.ViewRoom;

/**
 * Created by albazzy on 15/04/2017.
 */

public class Room {

    String image,name,desc,capacity,facility,floor;

    public Room(String image, String name, String desc , String capacity, String facility, String floor) {
        this.image = image;
        this.name = name;
        this.desc = desc;
        this.capacity = capacity;
        this.facility = facility;
        this.floor = floor;
    }

    public String getFloor() {
        return floor;
    }

    public void setFloor(String floor) {
        this.floor = floor;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getFacility() {
        return facility;
    }

    public void setFacility(String facility) {
        this.facility = facility;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
