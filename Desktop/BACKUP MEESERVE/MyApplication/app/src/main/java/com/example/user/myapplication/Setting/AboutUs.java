package com.example.user.myapplication.Setting;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.example.user.myapplication.R;

/**
 * Created by endis on 10/05/2017.
 */

public class AboutUs extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_us);
    }
}
