package com.example.user.myapplication.History.Summary;

import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.myapplication.Absence.BlankPages;
import com.example.user.myapplication.LainLain.connectionDetector;
import com.example.user.myapplication.Menu.menuActivity;
import com.example.user.myapplication.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import static com.example.user.myapplication.LainLain.URL.url;

public class SummaryActivity extends AppCompatActivity {
    connectionDetector cd;
    Boolean isInternetPresent = false;

    String tr_idpass;
    ProgressDialog pd;
    EditText mom;
    String summary,error;
    Button submit;
    LinearLayout absent;
    String TAG = "Error Upload";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        tr_idpass = intent.getStringExtra("trid");
        mom= (EditText) findViewById(R.id.mom);
        mom.setFilters(new InputFilter[]{new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                String regex = "a-z~@#$%^&*:;<>.,/}{+";
                if (!dest.toString().contains("") || !dest.toString().matches("[" + regex + "]+")) {
                    return null;
                }
                return source;
            }
        }});


        submit = (Button) findViewById(R.id.submit);


        absent = (LinearLayout) findViewById(R.id.absent);

        absent.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v) {
                cd = new connectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent intent = new Intent(SummaryActivity.this, BlankPages.class);
                    intent.putExtra("trid",tr_idpass);
                    startActivity(intent);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }
            }

            });

        submit.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                cd= new connectionDetector(getApplicationContext());
                isInternetPresent=cd.isConnectingToInternet();
                if (isInternetPresent){
                    summary = new String(mom.getText().toString());
                    kirimMoM(tr_idpass,summary);
                }else{
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }



//                Intent intent = new Intent(getApplicationContext(),MainActivity.class);
//                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                startActivity(intent);
            }
        });
    }
    private void kirimMoM(final String tr, String mm)
    {
        class momAsync extends AsyncTask<String,Void,JSONObject>
        {

            protected void onPreExecute()
            {
                super.onPreExecute();
                pd = new ProgressDialog(SummaryActivity.this);
                pd.setMessage("Loading...");
                pd.setIndeterminate(false);
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);
                pd.show();

            }

            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                String summary = params[1];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);
                    param.put("summary",summary);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"updateSummary2.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("response is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                    Log.d(TAG, "onPreExecute: " + ex.toString() );
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);
                pd.dismiss();
                if(result!=null)
                {
                    try {
                        JSONObject jsonObject = result.getJSONObject("update_summary");
                        error = jsonObject.getString("error_msg");
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                        Log.d(TAG, "onPostExecute: " + exz.toString() );

                    }
//                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        cekerror();
                    }
                }
            }


        momAsync async = new momAsync();
        async.execute(tr,mm);

    }


    public void cekerror(){
        if(error.equals("true")) {
            Intent intent = new Intent(SummaryActivity.this, menuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
//            startActivity(intent);
//            Calendar cal = Calendar.getInstance();
//            Intent intent = new Intent(Intent.ACTION_EDIT);
//            intent.setType("vnd.android.cursor.item/event");
//            intent.putExtra("beginTime", cal.getTimeInMillis());
//            intent.putExtra("allDay", false);
//            intent.putExtra("rrule", "FREQ=DAILY");
//            intent.putExtra("endTime", cal.getTimeInMillis()+60*60*1000);
//            intent.putExtra("title", "A Test Event from android app");
            startActivity(intent);

           // startActivity(intents);
            Toast.makeText(SummaryActivity.this,"Your Transaction Has Been Submitted",Toast.LENGTH_LONG).show();

        }
        else if(error.equals("gagal update"))
        {
            Toast.makeText(SummaryActivity.this,"Error Occured",Toast.LENGTH_LONG).show();
        }
        else {
            Toast.makeText(SummaryActivity.this,"Failed Upload" ,Toast.LENGTH_LONG).show();
        }

    }

}
