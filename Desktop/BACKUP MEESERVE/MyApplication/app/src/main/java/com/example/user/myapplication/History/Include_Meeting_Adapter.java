package com.example.user.myapplication.History;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by albazzy on 02/05/2017.
 */

public class Include_Meeting_Adapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context context;
    private LayoutInflater inflater;
    List<Include_Meeting_Model> data = Collections.emptyList();
    Include_Meeting_Model current;
    int currentPos = 0;

    public Include_Meeting_Adapter(List<Include_Meeting_Model> data, Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.include_container,parent,false);
        Include_Meeting_Adapter.MyHolder holder = new Include_Meeting_Adapter.MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final Include_Meeting_Adapter.MyHolder myholder = (Include_Meeting_Adapter.MyHolder)holder;
        final Include_Meeting_Model current = data.get(position);
        Picasso.with(context).load(current.image).fit().transform(new CircleTransform()).into(myholder.imageView);
        myholder.durasi.setText(current.durasi);
        myholder.Agenda.setText(current.Agenda);
        myholder.bookingtime.setText(current.booking_time);
        myholder.ruangan.setText(current.Room_name);
        myholder.Booker.setText(current.booker);
        myholder.rel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,Include_Details.class);
                i.putExtra("trid",current.tr_id);
                i.putExtra("tr_status",current);
                i.putExtra("Agenda",current);
                i.putExtra("starthour",current);
                i.putExtra("endhour",current);
                i.putExtra("roomname",current);
                i.putExtra("booked_date",current);
                i.putExtra("BookingNumber",current);
                i.putExtra("BookingDate",current);
                i.putExtra("summary",current);
                i.putExtra("Booker",current);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);

//                Toast.makeText(v.getContext(), (CharSequence) current, Toast.LENGTH_SHORT).show();
            }
        });








    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    class MyHolder extends RecyclerView.ViewHolder{

        TextView Agenda;
        TextView bookingtime;
        TextView Booker;
        TextView ruangan;
        TextView id;
        ImageView imageView;
        TextView durasi;
        RelativeLayout rel;


        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.room_include_image);
            Agenda= (TextView) itemView.findViewById(R.id.agendaINCLUDE);
            bookingtime = (TextView) itemView.findViewById(R.id.BOOKINGINCLUDE);
            ruangan = (TextView) itemView.findViewById(R.id.RoomnameINCLUDE);
            rel = (RelativeLayout)itemView.findViewById(R.id.include_rel);
            durasi = (TextView)itemView.findViewById(R.id.DURASIINCLUDE);
            Booker = (TextView)itemView.findViewById(R.id.booker);



        }

    }
}
