package com.example.user.myapplication.Setting;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.example.user.myapplication.LainLain.connectionDetector;
import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;

public class SettingActivity extends AppCompatActivity {

    connectionDetector cd;
    Boolean isInternetPresent = false;

    LinearLayout howToUse, aboutUs, logOut;

    UserSessionManager session;


    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting);


        //SESSION
        session = new UserSessionManager(getApplicationContext());


        howToUse = (LinearLayout) findViewById(R.id.howToUse);
        aboutUs = (LinearLayout) findViewById(R.id.aboutUs);
        logOut = (LinearLayout) findViewById(R.id.logOut);

        howToUse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new connectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent i1 = new Intent(SettingActivity.this, HowToUse.class);
                    startActivity(i1);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }

        });


        aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cd = new connectionDetector(getApplicationContext());
                isInternetPresent = cd.isConnectingToInternet();
                if (isInternetPresent) {
                    Intent i1 = new Intent(SettingActivity.this, AboutUs.class);
                    startActivity(i1);
                } else {
                    Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
                }

            }

        });

        logOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(SettingActivity.this);

                builder1.setMessage("Are you sure?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "No",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                builder1.setNegativeButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                session.logoutUser();
                                finishAffinity();
                            }
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }


        });
    }
}
