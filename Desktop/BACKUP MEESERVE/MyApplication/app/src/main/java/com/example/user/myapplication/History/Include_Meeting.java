package com.example.user.myapplication.History;


import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.example.user.myapplication.LainLain.URL.url;

/**
 * A simple {@link Fragment} subclass.
 */
public class Include_Meeting extends Fragment {
    public static final int CONNECTION_TIMEOUT = 20000;
    public static final int READ_TIMEOUT = 25000;
    private RecyclerView RecHistory;
    private Include_Meeting_Adapter mAdapter;
    String userID;
    SwipeRefreshLayout mSwipeRefreshLayout;
    String error;

    JSONArray jArray;

    UserSessionManager session;
    String userget;

    public Include_Meeting() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootviewz =  inflater.inflate(R.layout.fragment_include__meeting, container, false);
        RecHistory = (RecyclerView)rootviewz.findViewById(R.id.meetingrec);
        session = new UserSessionManager(getContext());
        HashMap<String, String> user = session.getUserDetails();
        userget = user.get(UserSessionManager.KEY_UserId);


        kirimuserid(userget);
        //Intent intent = getIntent();
        // userID=intent.getStringExtra("userid");
        mSwipeRefreshLayout = (SwipeRefreshLayout)rootviewz.findViewById(R.id.swipeRefresh3);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                kirimuserid(userget);
                // new AsyncFetch().execute();
            }
        });


        return rootviewz;
    }


    private void kirimuserid(final String uid)
    {
        class AsyncFetch extends AsyncTask<String, Void, JSONObject> {
            ProgressDialog pdLoading = new ProgressDialog(getActivity());
//        HttpURLConnection conn;
//        URL url = null;

            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                //this method will be running on UI thread
                pdLoading.setMessage("\tLoading...");
                pdLoading.setCancelable(false);
                pdLoading.show();

            }

            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String userid = params[0];
                ;
                JSONObject param = new JSONObject();
                try {
                    param.put("userid",userid);


                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"meetings.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("response is ", Response);

                    return new JSONObject(Response);

                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }


            //        @Override
            protected void onPostExecute(JSONObject result) {

                //this method will be running on UI thread

                pdLoading.dismiss();
                List<Include_Meeting_Model> data = new ArrayList<>();

                pdLoading.dismiss();
                    try {

                        jArray = result.getJSONArray("member");
                        //Intent c = new Intent(getContext(), Complete_Detail.class);

                        // Extract data from json and store into ArrayList as class objects
                        for(int i=0;i<jArray.length();i++){
                            JSONObject json_data = jArray.getJSONObject(i);
                            Include_Meeting_Model datas = new Include_Meeting_Model();
                            datas.Status= json_data.getString("tr_status");
                            datas.Agenda= json_data.getString("Agenda");
                            datas.booking_time= "Meeting Date : " + json_data.getString("BookingDate");
                            datas.booked_date="Booked at : "+ json_data.getString("Booked_Date");
                            datas.Room_name= "Room Name : " + json_data.getString("Room_Name");
                            datas.tr_id = json_data.getString("trid");
                            datas.Start_hour = "From: " + json_data.getString("starthour");
                            datas.end_hour = "To: " +  json_data.getString("endhour");
                            datas.bookingnumber = "Book Number : " + json_data.getString("BookingNumber");
                            datas.summary = json_data.getString("summary");
                            datas.booker = "Booker : "+  json_data.getString("Booker");
                            datas.image = json_data.getString("image");
                            datas.durasi = "Duration : " +json_data.getString("starthour").concat(" - ").concat(json_data.getString("endhour"));
                            error = json_data.getString("error_msg");
                            data.add(datas);


                        }

                        // Setup and Handover data to recyclerview

                        mAdapter = new Include_Meeting_Adapter(data,getActivity().getApplicationContext());
                        RecHistory.setAdapter(mAdapter);
                        RecHistory.setLayoutManager(new LinearLayoutManager(getActivity().getApplicationContext()));
                        mSwipeRefreshLayout.setRefreshing(false);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }



        AsyncFetch async = new AsyncFetch();
        async.execute(uid);

    }




    public void cekerror()
    {
        if(error.equals("false"))
            Toast.makeText(getContext().getApplicationContext(),"Anda belum mengikuti meeting apapun",Toast.LENGTH_LONG).show();


    }


}