package com.example.user.myapplication.BookiingDetail;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.user.myapplication.R;

//import com.example.user.myapplication.BookiingDetail.RoundImage;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by AASUS on 31/03/2017.
 */

public class ListViewAdapterBooking extends ArrayAdapter<ListViewModelBooking>  {
    // Declare Variables
    private Activity activity;
    private List<ListViewModelBooking> modelBooking;

    private LayoutInflater inflanter;
    Context context;
    int resource;
    HashMap<String, String> resultp = new HashMap<String, String>();

    String mulai, selesai, date;


    public ListViewAdapterBooking(Context applicationContext, int resource, ArrayList<ListViewModelBooking> modelBooking) {
        super(applicationContext, resource, modelBooking);
        this.context = applicationContext;
        this.resource = resource;
        this.modelBooking = modelBooking;
    }


    @Override
    public ListViewModelBooking getItem(int position) {
        return modelBooking.get(position);
    }
    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup Parent){
        if(convertView == null){
            inflanter = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            convertView = inflanter.inflate(R.layout.listsingle_static, null, true);
        }

//        Intent i = ((Activity)context).getIntent();
//        mulai = i.getStringExtra("start");
//        selesai = i.getStringExtra("end");
//        date = i.getStringExtra("date");

//        ListView lv = (ListView) convertView.findViewById(R.id.listViewBooking);
        TextView roomName = (TextView) convertView.findViewById(R.id.roomName);
        TextView roomFloor = (TextView) convertView.findViewById(R.id.roomFloor);
        TextView roomFacility = (TextView) convertView.findViewById(R.id.roomFacility);
        TextView roomCapacity = (TextView) convertView.findViewById(R.id.roomCapacity);
        ImageView roomImage = (ImageView) convertView.findViewById(R.id.roomImage);

        ListViewModelBooking bookingmodel = modelBooking.get(position);
        roomName.setText(bookingmodel.getRoomName());
        roomFloor.setText(bookingmodel.getRoomFloor());
        roomFacility.setText(bookingmodel.getRoomFacility());
        roomCapacity.setText(bookingmodel.getRoomCapacity());
        Picasso.with(context).load(bookingmodel.getRoomImage()).into(roomImage);


        return convertView;
    }
}
