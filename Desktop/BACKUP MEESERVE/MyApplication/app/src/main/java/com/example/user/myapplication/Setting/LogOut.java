package com.example.user.myapplication.Setting;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.user.myapplication.R;
import com.example.user.myapplication.Session.UserSessionManager;

/**
 * Created by endis on 10/05/2017.
 */

public class LogOut extends AppCompatActivity {

    UserSessionManager session;
    Button yes,no;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.alert_logout);

        //SESSION
        session = new UserSessionManager(getApplicationContext());
        //untuk logout
        yes=(Button) findViewById(R.id.dialog_yes);
        yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                session.logoutUser();
                finishAffinity();
            }
        });


        no=(Button) findViewById(R.id.dialog_no);

    }
}
