package com.example.user.myapplication.History;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.myapplication.R;
import com.squareup.picasso.Picasso;

import java.util.Collections;
import java.util.List;

/**
 * Created by albazzy on 25/04/2017.
 */

public class Booked_Adapter  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    String ID_PASS;
    private Context context;
    private LayoutInflater inflater;
    List<BookedModel> data = Collections.emptyList();
    BookedModel current;
    int currentPos = 0;

    public Booked_Adapter(List<BookedModel> data, Context context) {
        this.context = context;
        inflater = LayoutInflater.from(context);
        this.data = data;
    }



    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.booked_container,parent,false);
        MyHolder holder = new MyHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final MyHolder myholder = (MyHolder)holder;
        final BookedModel current = data.get(position);
        Picasso.with(context).load(current.Image).fit().transform(new CircleTransform()).into(myholder.imageView);
        myholder.ruangan.setText(current.Room_name);
        myholder.Agenda.setText(current.Agenda);
        myholder.bookingtime.setText(current.booking_time);
        myholder.durasi.setText(current.durasi);
        myholder.rel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent i = new Intent(context,BookedDetail.class);
                i.putExtra("trid",current.tr_id);
                i.putExtra("tr_status",current);
                i.putExtra("Agenda",current);
                i.putExtra("starthour",current);
                i.putExtra("endhour",current);
                i.putExtra("roomname",current);
                i.putExtra("booked_date",current);
                i.putExtra("BookingNumber",current);
                i.putExtra("BookingDate",current);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

//                Toast.makeText(v.getContext(), (CharSequence) current, Toast.LENGTH_SHORT).show();
            }
        });







    }

    @Override
    public int getItemCount() {
        return data.size();
    }



    class MyHolder extends RecyclerView.ViewHolder{

        TextView Status;
        TextView Agenda;
        TextView bookingtime;
        TextView bookedate;
        TextView ruangan;
        TextView id;
        TextView durasi;
        ImageView imageView;
        RelativeLayout rel;



        // create constructor to get widget reference
        public MyHolder(View itemView) {
            super(itemView);
            imageView = (ImageView)itemView.findViewById(R.id.RoomnameBookedImage);
            ruangan= (TextView) itemView.findViewById(R.id.RoomnameBooked);
            Agenda= (TextView) itemView.findViewById(R.id.agendabooked);
            durasi = (TextView)itemView.findViewById(R.id.durasiboooked);
            bookingtime = (TextView) itemView.findViewById(R.id.bookingbooked);
            rel = (RelativeLayout)itemView.findViewById(R.id.bookrel);


        }

    }
}
