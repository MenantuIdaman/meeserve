package com.example.user.myapplication.History;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.user.myapplication.R;

import java.util.ArrayList;

/**
 * Created by albazzy on 25/04/2017.
 */

public class AttendanceAdaptor extends ArrayAdapter<AttendanceModel> {

    ArrayList<AttendanceModel> data;
    Context context;
    int resource;


    public AttendanceAdaptor(@NonNull Context context, @LayoutRes int resource, @NonNull ArrayList<AttendanceModel> data) {
        super(context, resource, data);
        this.context = context;
        this.resource = resource;
        this.data = data;
    }



    public View getView(int postition, View convertView, ViewGroup parent) {
        if (convertView == null) {

            LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            convertView = inflater.inflate(R.layout.attendee_list, null, true);
        }

        TextView txtname = (TextView) convertView.findViewById(R.id.nama);
        TextView txtemail = (TextView) convertView.findViewById(R.id.email);
        TextView txtposisi = (TextView) convertView.findViewById(R.id.Posisi);
        TextView txtdesk = (TextView) convertView.findViewById(R.id.deskripsi);

        AttendanceModel listViewModelAttendance = data.get(postition);

        txtname.setText(listViewModelAttendance.getAttName());
        txtposisi.setText(listViewModelAttendance.getAttPosisi());
        txtemail.setText(listViewModelAttendance.getAttEmail());
        txtdesk.setText(listViewModelAttendance.getAttDesc());

        return convertView;
    }
}