package com.example.user.myapplication.BookingRoomVerif;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.myapplication.BookiingDetail.RoundImage;
import com.example.user.myapplication.InvitePeople.InviteActivity;
import com.example.user.myapplication.LainLain.connectionDetector;
import com.example.user.myapplication.R;
import com.squareup.picasso.Picasso;

import static com.example.user.myapplication.R.id.roomImage;

/**
 * Created by endis on 13/04/2017.
 */

public class BookingRoomVerifActivity extends AppCompatActivity {

    connectionDetector cd;
    Boolean isInternetPresent = false;


    Context context;
    String mulai, selesai,date,userid,image,roomName,roomFacility,roomDesc,roomID,roomCapacity;
    String a;
    RoundImage roundimage1;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.room_of_bookingdetail);
        Toolbar toolbar = (Toolbar)findViewById(R.id.BookinRoomTool);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Room's Detail");
        toolbar.setTitleTextColor(Color.WHITE);
        mProgressDialog = new ProgressDialog(BookingRoomVerifActivity.this);
        mProgressDialog.setMessage("Loading...");
        mProgressDialog.setIndeterminate(false);
        mProgressDialog.show();
        mProgressDialog.dismiss();

        Intent intent = getIntent();
        mulai = intent.getStringExtra("start");
        selesai = intent.getStringExtra("end");
        date = intent.getStringExtra("date");
        //userid=intent.getStringExtra("userid");
        roomID=intent.getStringExtra("RoomID");
        roomName=intent.getStringExtra("Room_Name");
        roomFacility=intent.getStringExtra("room_fac");
        roomCapacity = intent.getStringExtra("room_caps");
        roomDesc = intent.getStringExtra("room_desc");
        image = intent.getStringExtra("image");
        //Toast.makeText(getApplicationContext(), userid+"\n"+mulai+"\n"+selesai+"\n"+date+"\n"+"\n"+roomID+"\n"+roomName+"\n"+roomFacility+"\n"+image, Toast.LENGTH_LONG).show();
//        Toast.makeText(getApplicationContext(),roomCapacity,Toast.LENGTH_LONG).show();

        Button book = (Button) findViewById(R.id.buttonBook);
        TextView room_name = (TextView) findViewById(R.id.roomName);
        TextView room_facility = (TextView) findViewById(R.id.roomFacility);
        TextView room_capacity = (TextView) findViewById(R.id.roomCapacity);
        ImageView room_image = (ImageView) findViewById(roomImage);

        Bundle bd = intent.getExtras();

        if (bd!=null){
            String getRoomName=(String) bd.get("Room_Name");
            room_name.setText(getRoomName);

            String getRoomFacility=(String) bd.get("room_fac");
            room_facility.setText(getRoomFacility);

            String getRoomCaps=(String) bd.get("room_caps");
            room_capacity.setText(getRoomCaps);

            Picasso.with(context).load(image).into(room_image);
        }



        book.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
//                AlertDialogg.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                cd= new connectionDetector(getApplicationContext());
                isInternetPresent=cd.isConnectingToInternet();
                if (isInternetPresent){
                    Intent i = new Intent(getApplicationContext(), InviteActivity.class);
                    i.putExtra("start", mulai);
                    i.putExtra("end", selesai);
                    i.putExtra("bookingdate", date);
                    i.putExtra("room_caps",roomCapacity);
                    i.putExtra("roomid",roomID);
                    startActivity(i);

                }else{
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }

            }

        });
    }






}
