package com.example.user.myapplication.BookiingDetail;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.example.user.myapplication.BookingRoomVerif.BookingRoomVerifActivity;
import com.example.user.myapplication.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import static android.widget.AdapterView.OnItemClickListener;
import static com.example.user.myapplication.LainLain.URL.url;

/**
 * Created by endis on 10/04/2017.
 */

public class ListviewBooking extends Activity{
    // Declare Variables
    JSONObject jsonobject;
    JSONArray jsonarray;
    ListView listview;


    ProgressDialog mProgressDialog;
    ArrayList<ListViewModelBooking> list_dataBooking ;
    private  ListViewAdapterBooking adapterBooking;

    RoundImage roundimage;
    ImageView roomImage;
    String ri;

    String mulai, selesai,date,error;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Get the view from listview_main.xml
        setContentView(R.layout.activity_bookings_detail);
        Toolbar toolbar=(Toolbar)findViewById(R.id.toolbar);
        toolbar.setTitle("Please Select Your Room");
        toolbar.setTitleMarginStart(10);

//        adapterBooking = new ListViewAdapterBooking(this,modelbooking);
        listview = (ListView)findViewById(R.id.listviewRoom);
        listview.setAdapter(adapterBooking);
        Intent intent = getIntent();
        mulai = intent.getStringExtra("start");
        selesai = intent.getStringExtra("end");
        date = intent.getStringExtra("date");
        //userid = intent.getStringExtra("userid");
        kirimTanggal(mulai,selesai,date);
        list_dataBooking = new ArrayList<>();




        listview.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ListViewModelBooking bookingmodel = list_dataBooking.get(position);
                Intent i = new Intent(getApplicationContext(), BookingRoomVerifActivity.class);
                i.putExtra("start", mulai);
                i.putExtra("end", selesai);
                i.putExtra("date", date);
                //i.putExtra("userid",userid);
                i.putExtra("RoomID",bookingmodel.getRoomId());
                i.putExtra("Room_Name",bookingmodel.getRoomName());
                i.putExtra("room_desc",bookingmodel.getRoomDesc());
                i.putExtra("room_fac",bookingmodel.getRoomFacility());
                i.putExtra("room_caps",bookingmodel.getRoomCapacity());
                i.putExtra("image",bookingmodel.getRoomImage());

                startActivity(i);

            }
        });

    }

    private void kirimTanggal(final String ml, String sls,String tgl) {
        class LoginAsync extends AsyncTask<String, Void, JSONObject> {
            private Dialog loadingDialog;

            @Override
            protected void onPreExecute(){
                super.onPreExecute();
                mProgressDialog = new ProgressDialog(ListviewBooking.this);
                mProgressDialog.setMessage("Loading...");
                mProgressDialog.setCancelable(false);
                mProgressDialog.setCanceledOnTouchOutside(false);
                mProgressDialog.show();
            }

            @Override
            protected JSONObject doInBackground(String...params){
                String responce;
                String st = params[0];
                String ed = params[1];
                String dt = params[2];

                JSONObject param = new JSONObject();

                try {

                    //                    param.put("sessionId", sid);
                    param.put("start", st);
                    param.put("end", ed);
                    param.put("date", dt);

                } catch (Exception ex) {

                }
                String message="";

                message=param.toString();
                Log.d("header",param.toString() );

                try{
                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"selectRoomByDate.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-type", "application/json");

                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();

                    responce = EntityUtils.toString(entity);
                    Log.d("response is", responce);

                    return new JSONObject(responce);

                    //                    ======================================

                }catch (Exception e){
                    e.printStackTrace();
                }

                return null;

            }


            @Override
                    protected void onPostExecute(JSONObject result) {
                super.onPostExecute(result);
                mProgressDialog.dismiss();
                if(result != null){

                    try {

                        jsonarray = result.getJSONArray("room_data_bydate");

                        for (int i = 0; i < jsonarray.length(); i++) {
                            JSONObject list = jsonarray.getJSONObject(i);
                            list_dataBooking.add(new ListViewModelBooking(
                                    list.getString("Room_Name"),
                                    list.getString("room_floor"),
                                    list.getString("room_desc"),
                                    list.getString("image"),
                                    list.getString("RoomID"),
                                    list.getString("room_fac"),
                                    list.getString("room_caps")

                            ));
                        }
//
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    ListViewAdapterBooking adapter = new ListViewAdapterBooking(getApplicationContext(),R.layout.listsingle_static, list_dataBooking);
                    listview.setAdapter(adapter);
                }
                //cekerror();
            }
        }
        LoginAsync reg = new LoginAsync();
        reg.execute(ml, sls, tgl);
    }



    public void cekerror(){
        if (error.equals("false")){
            Toast.makeText(getApplicationContext(), "Room not available", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(ListviewBooking.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }



}


