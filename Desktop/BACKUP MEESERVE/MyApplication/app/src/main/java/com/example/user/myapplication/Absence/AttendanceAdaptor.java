package com.example.user.myapplication.Absence;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.myapplication.R;

import java.util.Collections;
import java.util.List;

/**
 * Created by albazzy on 08/05/2017.
 */

public class AttendanceAdaptor extends RecyclerView.Adapter<RecyclerView.ViewHolder>{

    private Context context;
    private LayoutInflater inflater;
    List<AttendanceModel> data = Collections.emptyList();
    AttendanceModel current;

    int currentPos = 0;

    public AttendanceAdaptor(List<AttendanceModel>data,Context context)
    {
        this.data = data;
        inflater = LayoutInflater.from(context);
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.container,parent,false);
        final MyHolder holder = new MyHolder(view);

        return holder;
    }



    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final MyHolder myHolder = (MyHolder) holder;
        final AttendanceModel current = data.get(position);
        myHolder.nama.setText(current.attName);
        myHolder.email.setText(current.attEmail);
        myHolder.pos.setText(current.attPosisi);
        myHolder.desc.setText(current.attDesc);
        myHolder.trid.setText(current.trid);
        myHolder.pid.setText(current.peopleid);

//        myHolder.rel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(context,"Touch "+ current.peopleid, Toast.LENGTH_SHORT ).show();
//            }
//        });


    }




    @Override
    public int getItemCount() {
        return data.size();
    }




    public class MyHolder extends RecyclerView.ViewHolder{

        TextView nama;
        TextView desc;
        TextView pos;
        TextView email;
        TextView trid;
        TextView pid;
        RelativeLayout rel;




        public MyHolder(View itemView) {
            super(itemView);
            nama = (TextView)itemView.findViewById(R.id.nama);
            desc = (TextView) itemView.findViewById(R.id.status);
            pos = (TextView) itemView.findViewById(R.id.posisi);
            email = (TextView) itemView.findViewById(R.id.email);
            rel = (RelativeLayout) itemView.findViewById(R.id.rel);
            trid = (TextView)itemView.findViewById(R.id.trid);
            pid = (TextView)itemView.findViewById(R.id.pid);
        }
    }
}
