package com.example.user.myapplication.Absence;

import java.io.Serializable;

/**
 * Created by albazzy on 08/05/2017.
 */

public class AttendanceModel implements Serializable{

    String attName;
    String attEmail;
    String attPosisi;
    String attDesc;
    String trid;
    String peopleid;

    public String getAttName() {
        return attName;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }

    public String getAttEmail() {
        return attEmail;
    }

    public void setAttEmail(String attEmail) {
        this.attEmail = attEmail;
    }

    public String getAttPosisi() {
        return attPosisi;
    }

    public void setAttPosisi(String attPosisi) {
        this.attPosisi = attPosisi;
    }

    public String getAttDesc() {
        return attDesc;
    }

    public void setAttDesc(String attDesc) {
        this.attDesc = attDesc;
    }

    public String getTrid() {
        return trid;
    }

    public void setTrid(String trid) {
        this.trid = trid;
    }

    public String getPeopleid() {
        return peopleid;
    }

    public void setPeopleid(String peopleid) {
        this.peopleid = peopleid;
    }
}
