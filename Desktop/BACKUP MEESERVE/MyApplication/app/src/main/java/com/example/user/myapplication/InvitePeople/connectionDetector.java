package com.example.user.myapplication.InvitePeople;

/**
 * Created by AASUS on 05/04/2017.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by endis on 17/03/2017.
 */

public class connectionDetector {
    private Context _context;

    public connectionDetector(Context context) {
        this._context = context;
    }

    public boolean isConnectingToInternet(){
        ConnectivityManager connectivity = (ConnectivityManager) _context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if(connectivity != null)
        {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if(info != null)
                for (int i = 0;i<info.length;i++)
                    if(info[i].getState() == NetworkInfo.State.CONNECTED)
                    {
                        return true;
                    }
        }
        return false;
    }
}
