package com.example.user.myapplication.BookiingDetail;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.user.myapplication.InvitePeople.connectionDetector;
import com.example.user.myapplication.R;

import org.w3c.dom.Text;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.TimeZone;

/**
 * Created by endis on 07/03/2017.
 */

public class detailActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener{

    connectionDetector cd;
    Boolean isInternetPresent = false;



    final Context context = this;
    long hours = new Time(System.currentTimeMillis()).getTime();
    Button  jamMulai, jamSelesai, submit;
    TextView tampilTanggal, tampilJamMulai, tampilJamSelesai;
    private int mYear, mMonth, mDay, mHour, mMinute;
    LinearLayout pickdates;

    Spinner start,end;
    String kirimMulai,kirimSelesai;


    String mulai,selesai,date,userid;

//    private static final String[]paths = {"All", "Shift 1", "Shift 2"};
    List<HashMap<String, String>> list_data = new ArrayList<HashMap<String, String>>();
    HashMap<String, String> map;







    @Override
    public void onBackPressed() {
        super.onBackPressed();
        detailActivity.this.finish();
        return;
    }


    protected void onPause(){
        super.onPause();
        // fetch it as appropriate
        tampilTanggal.setError(null);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail);

        submit = (Button) findViewById(R.id.submit);
        pickdates=(LinearLayout)findViewById(R.id.pickdates);
        tampilTanggal = (TextView) findViewById(R.id.tampilTanggal);
        pickdates.setOnClickListener(this);


        start = (Spinner) findViewById(R.id.jamMulai);
        end = (Spinner)findViewById(R.id.jamSelesai);
        start.setOnItemSelectedListener(this);
        end.setOnItemSelectedListener(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbardetail);
        setSupportActionBar(toolbar);
        toolbar.setTitle("Room Reservation");
        toolbar.setTitleTextColor(Color.WHITE);

        Intent intent = getIntent();
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //userid= intent.getStringExtra("userid");

        //Toast.makeText(getApplicationContext(),userid, Toast.LENGTH_LONG).show();

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
                date = tampilTanggal.getText().toString();

                cd= new connectionDetector(getApplicationContext());
                isInternetPresent=cd.isConnectingToInternet();
                if (isInternetPresent){
                    if (tampilTanggal.getText().toString().equals("")){
                        tampilTanggal.setError("Date must be filled");
                        Toast.makeText(getApplicationContext(),"Date must be filled",Toast.LENGTH_SHORT).show();
                    }else {
                        Intent intent = new Intent(detailActivity.this, ListviewBooking.class);
                        intent.putExtra("start", kirimMulai);
                        intent.putExtra("end", kirimSelesai);
                        intent.putExtra("date", date);
                        startActivity(intent);
                    }

                }else{
                    Toast.makeText(getApplicationContext(),"No Internet Connection",Toast.LENGTH_LONG).show();
                }



            }
        });

        //buat spinner
        List<String> categories = new ArrayList<String>();
        for(int i=9;i<=16;i++){
            categories.add(String.valueOf(i).concat(":00"));
        }
        //Toast.makeText(getApplicationContext(),mulai,Toast.LENGTH_LONG).show();
        //Toast.makeText(getApplicationContext(),kirimMulai,Toast.LENGTH_LONG).show();



        final ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, categories);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        start.setAdapter(dataAdapter);



        start.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                mulai = String.valueOf(dataAdapter.getItem(position));
                kirimMulai= mulai.substring(0, mulai.length() - 3);
                //Toast.makeText(getApplicationContext(),kirimMulai,Toast.LENGTH_LONG).show();

                String item = parent.getItemAtPosition(position).toString();
                String[]a=item.split(":");
                String ab=a[0];
                position=Integer.parseInt(ab);

                List<String> categoriez2 = new ArrayList<String>();
                for (int i = position+1; i <18; i++) {
                    categoriez2.add(String.valueOf(i).concat(":00"));
                }

                final ArrayAdapter<String> dataAdapters = new ArrayAdapter<String>(detailActivity.this, android.R.layout.simple_spinner_item, categoriez2);
                dataAdapters.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                end.setAdapter(dataAdapters);

                end.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        selesai = String.valueOf(dataAdapters.getItem(position));
                        kirimSelesai = selesai.substring(0, selesai.length() - 3);

                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });



    }


    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public void onClick(View v) {
        if (v == pickdates) {
            tampilTanggal.setError(null);
            //menentukan tanggal yang mau dipilih
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
                @Override
                public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                    tampilTanggal.setText(dayOfMonth + "/" + (month + 1) + "/" + year);
                }
            }, mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();

        }
        if (v == jamMulai) {
            //menentukan jam mulai yang mau dipilih
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            //menjalankan timepicker
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    tampilJamMulai.setText(hourOfDay + ":" + minute);
                }
            }, mHour, mMinute, false);
            timePickerDialog.show();
        }

        if (v == jamSelesai) {
            //menentukan jam mulai yang mau dipilih
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            //menjalankan timepicker
            TimePickerDialog timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                    tampilJamSelesai.setText(hourOfDay + ":" + minute);
                }
            }, mHour, mMinute, false);
            timePickerDialog.show();
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(detailActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }

    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}