package com.example.user.myapplication.History;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.myapplication.History.Summary.SummaryActivity;
import com.example.user.myapplication.Menu.menuActivity;
import com.example.user.myapplication.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import static com.example.user.myapplication.LainLain.URL.url;

public class BookedDetail extends AppCompatActivity {
    String error;
    String cancel;
    String tr_idpass;
    TextView textView ;
    ProgressDialog pd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booked_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        cancel = "Your transaction has been canceled";
        Intent intent = getIntent();
        tr_idpass = intent.getStringExtra("trid");

        RelativeLayout submit = (RelativeLayout) findViewById(R.id.ButtonSubmit);
        RelativeLayout cancel = (RelativeLayout)findViewById(R.id.Cancel_Button);





        if (getIntent().getSerializableExtra("tr_status") != null) {
            BookedModel history = (BookedModel) getIntent().getSerializableExtra("tr_status");


            TextView status2 = (TextView) findViewById(R.id.status2);
            status2.setText(history.Status);
            TextView agenda = (TextView) findViewById(R.id.agenda2);
            agenda.setText(history.Agenda);
            final TextView start = (TextView) findViewById(R.id.starthour);
            start.setText(history.Start_hour);
            TextView end = (TextView) findViewById(R.id.endhour);
            end.setText(history.end_hour);
            TextView booked = (TextView) findViewById(R.id.booked2);
            booked.setText(history.booked_date);
            TextView bookingnum = (TextView) findViewById(R.id.bookingnumber);
            bookingnum.setText(history.bookingnumber);
            TextView bookingdate = (TextView) findViewById(R.id.bookingdates1);
            bookingdate.setText(history.booking_time);
            TextView roomname = (TextView) findViewById(R.id.ruangan2);
            roomname.setText(history.Room_name);
            TextView id = (TextView) findViewById(R.id.IdPass);
            id.setText(history.tr_id);


        }
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(),SummaryActivity.class);
                intent.putExtra("trid",tr_idpass);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelBooking(tr_idpass);
            }
        });

    }

    private void cancelBooking(final String tr)
    {
        class cancelAsync extends AsyncTask<String,Void,JSONObject>
        {

            protected void onPreExecute()
            {
                super.onPreExecute();
                pd = new ProgressDialog(BookedDetail.this);
                pd.setMessage("Loading...");
                pd.setIndeterminate(false);
                pd.show();
                pd.setCancelable(false);
                pd.setCanceledOnTouchOutside(false);

            }

            protected JSONObject doInBackground(String... params)
            {
                String Response;
                String trid = params[0];
                JSONObject param = new JSONObject();
                try {
                    param.put("trid",trid);

                }catch (Exception e)
                {
                    e.printStackTrace();
                }
                String message = "";
                message = param.toString();
                Log.d("Header",param.toString());

                try{

                    HttpClient httpClient = new DefaultHttpClient();
                    HttpPost httpPost = new HttpPost(url+"cancelBooking2.php");
                    httpPost.setEntity(new StringEntity(message,"UTF8"));
                    httpPost.setHeader("Content-Type","application/json");
                    StringEntity se = new StringEntity(param.toString());
                    httpPost.setEntity(se);
                    HttpResponse response = httpClient.execute(httpPost);
                    HttpEntity entity = response.getEntity();
                    Response = EntityUtils.toString(entity);
                    Log.d("response is ", Response);

                    return new JSONObject(Response);
                }catch (Exception ex)
                {
                    ex.printStackTrace();
                }
                return null;
            }

            protected void onPostExecute(JSONObject result)
            {
                super.onPostExecute(result);
                if(result!=null)
                {
                    try {
                        JSONObject jsonObject = result.getJSONObject("cancel_data");
                        error = jsonObject.getString("error_msg");
                    }catch (Exception exz)
                    {
                        exz.printStackTrace();
                    }
                    cekerror();
                }
            }

        }
        cancelAsync async = new cancelAsync();
        async.execute(tr);

    }

    public void cekerror(){
        if(error.equals("true")) {
            Intent intent = new Intent(getApplicationContext(),menuActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            Toast.makeText(getApplicationContext(),"Your transaction has been canceled",Toast.LENGTH_LONG).show();
            startActivity(intent);



            //custom toast yang ada gambarnya

//            LayoutInflater layoutInflater = getLayoutInflater();
//            View view = layoutInflater.inflate(R.layout.custom_toast_layout,(ViewGroup)findViewById(R.id.custom_toast));
//
//
//
//            Toast toast = new Toast(getApplicationContext());
//            toast.setGravity(Gravity.CENTER_VERTICAL,0,0);
//            toast.setDuration(Toast.LENGTH_LONG);
//            toast.setView(view);
//            toast.show();



        }
        else
        {
            Toast.makeText(BookedDetail.this,"Error Occured",Toast.LENGTH_LONG).show();
        }

    }


}