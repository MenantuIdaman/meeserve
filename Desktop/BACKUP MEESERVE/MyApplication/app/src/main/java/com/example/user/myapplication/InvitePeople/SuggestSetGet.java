package com.example.user.myapplication.InvitePeople;

/**
 * Created by Ronny on 4/19/2017.
 */

public class SuggestSetGet {
    String id,name;
    public SuggestSetGet(String id, String name){
        this.setId(id);
        this.setName(name);
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}