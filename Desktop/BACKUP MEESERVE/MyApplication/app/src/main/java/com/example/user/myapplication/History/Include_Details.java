package com.example.user.myapplication.History;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.user.myapplication.R;

/**
 * Created by albazzy on 02/05/2017.
 */

public class Include_Details extends AppCompatActivity {
    String error;
    String tr_idpass;
    ProgressDialog pd;
    RelativeLayout button;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_include_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        button = (RelativeLayout) findViewById(R.id.daftarattend);
       getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Intent intent = getIntent();
        tr_idpass = intent.getStringExtra("trid");

        if (getIntent().getSerializableExtra("tr_status") != null) {
            Include_Meeting_Model history = (Include_Meeting_Model) getIntent().getSerializableExtra("tr_status");


            TextView status2 = (TextView) findViewById(R.id.statusz);
            status2.setText(history.Status);
            TextView agenda = (TextView) findViewById(R.id.agenda2z);
            agenda.setText(history.Agenda);
            final TextView start = (TextView) findViewById(R.id.starthourz);
            start.setText(history.Start_hour);
            TextView end = (TextView) findViewById(R.id.endhourz);
            end.setText(history.end_hour);
            TextView booked = (TextView) findViewById(R.id.bookedz);
            booked.setText(history.booked_date);
            TextView bookingnum = (TextView) findViewById(R.id.bookingnumberz);
            bookingnum.setText(history.bookingnumber);
            TextView bookingdate = (TextView) findViewById(R.id.bookingdates1z);
            bookingdate.setText(history.booking_time);
            TextView roomname = (TextView) findViewById(R.id.ruanganz);
            roomname.setText(history.Room_name);
            TextView id = (TextView) findViewById(R.id.IdPassz);
            id.setText(history.tr_id);
            TextView mom = (TextView)findViewById(R.id.MOMz);
            mom.setText(history.summary);

        }
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent z = new Intent(Include_Details.this,Attendee_List.class);
                z.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                z.putExtra("trid",tr_idpass);
                startActivity(z);
            }
        });


    }
}
