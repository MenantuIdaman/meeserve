package com.example.user.myapplication.History;

import java.util.HashMap;

/**
 * Created by albazzy on 25/04/2017.
 */

public class AttendanceModel extends HashMap<String,String> {
    private String attName;
    private String attEmail;
    private String attPosisi;
    private String attDesc;

    public AttendanceModel(String attName, String attEmail, String attPosisi, String attDesc) {
        this.attName = attName;
        this.attEmail = attEmail;
        this.attPosisi = attPosisi;
        this.attDesc = attDesc;
    }

    public String getAttName() {
        return attName;
    }

    public void setAttName(String attName) {
        this.attName = attName;
    }

    public String getAttEmail() {
        return attEmail;
    }

    public void setAttEmail(String attEmail) {
        this.attEmail = attEmail;
    }

    public String getAttPosisi() {
        return attPosisi;
    }

    public void setAttPosisi(String attPosisi) {
        this.attPosisi = attPosisi;
    }

    public String getAttDesc() {
        return attDesc;
    }

    public void setAttDesc(String attDesc) {
        this.attDesc = attDesc;
    }
}