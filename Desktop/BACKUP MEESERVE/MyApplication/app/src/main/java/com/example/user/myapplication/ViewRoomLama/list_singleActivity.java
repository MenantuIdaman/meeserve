package com.example.user.myapplication.ViewRoomLama;

import android.icu.text.SimpleDateFormat;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;

import com.example.user.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by endis on 15/03/2017.
 */

public class list_singleActivity extends AppCompatActivity {

    TextView txt1,txt2;
    ImageView img;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_single);

        txt1 = (TextView) findViewById(R.id.txt1);
        txt2 = (TextView) findViewById(R.id.txt2);
        img = (ImageView) findViewById(R.id.img);


        Intent myIntent = getIntent();
        //String image= myIntent.getStringExtra("image");
        //img.setImageBitmap();

        String roomName = myIntent.getStringExtra("name");
        txt1.setText(roomName);

        String roomDesc = myIntent.getStringExtra("desc");
        txt2.setText(roomDesc);
    }

//    private ListView mListView;
//
//    private List<HashMap<String, String>> mAndroidMapList = new ArrayList<>();
//    private static final String rid = "roomid";
//    private static final String rn  = "name";
//    private static final String fl = "floor";
//
//    private void loadListView() {
//
//        ListAdapter adapter = new SimpleAdapter(list_singleActivity.this, mAndroidMapList, R.layout.list_single,
//                new String[] { rid, rn, fl },
//                new int[] { R.id.img,R.id.txt1, R.id.txt2 });
//
//        mListView.setAdapter(adapter);
//
//    }




}
