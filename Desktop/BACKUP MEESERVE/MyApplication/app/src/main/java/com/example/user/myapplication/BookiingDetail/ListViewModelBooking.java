package com.example.user.myapplication.BookiingDetail;

import java.util.HashMap;

/**
 * Created by endis on 12/04/2017.
 */

public class ListViewModelBooking extends HashMap<String, String> {
    private String roomName;
    private String roomFloor;
    private String roomDesc;
    private String roomImage;
    private String roomId;
    private String roomFacility;
    private String roomCapacity;

    public ListViewModelBooking(){

    }

    public ListViewModelBooking(String roomName,String roomFloor,String roomDesc, String roomImage, String roomId,String roomFacility,String roomCapacity){
        this.roomName = roomName;
        this.roomFloor = roomFloor;
        this.roomDesc = roomDesc;
        this.roomImage = roomImage;
        this.roomId = roomId;
        this.roomFacility= roomFacility;
        this.roomCapacity=roomCapacity;
    }

    public void setRoomName(String roomName){
        this.roomName = roomName;
    }

    public String getRoomName(){
        return roomName;
    }

    public void setRoomFloor(String roomFloor){
        this.roomFloor = roomFloor;
    }

    public String getRoomFloor(){
        return roomFloor;
    }

    public void setRoomDesc(String roomDesc){
        this.roomDesc = roomDesc;
    }

    public String getRoomDesc(){
        return roomDesc;
    }

    public void setRoomImage(String roomImage){
        this.roomImage = roomImage;
    }

    public String getRoomImage(){
        return roomImage;
    }

    public void setRoomId(String roomId){
        this.roomId = roomId;
    }

    public String getRoomId(){
        return roomId;
    }

    public void setRoomFacility(String roomFacility){
        this.roomFacility = roomFacility;
    }

    public String getRoomFacility(){
        return roomFacility;
    }

    public void setRoomCapacity (String roomCapacity){this.roomCapacity = roomCapacity; }

    public String getRoomCapacity(){return  roomCapacity;}

}

